package models;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by leonid on 27.02.2015.
 */
@Entity
public class Heading {
    private Integer id;
    private String name;
    private Long themeCount;
    private Collection<Topic> topicsById;

    @Id
    @SequenceGenerator(name="heading_id_gen", sequenceName="heading_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE ,generator="heading_id_gen")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "theme_count", columnDefinition = "int default 0")
    public Long getThemeCount() {
        return themeCount;
    }

    public void setThemeCount(Long themeCount) {
        this.themeCount = themeCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Heading heading = (Heading) o;

        if (id != null ? !id.equals(heading.id) : heading.id != null) return false;
        if (name != null ? !name.equals(heading.name) : heading.name != null) return false;
        if (themeCount != null ? !themeCount.equals(heading.themeCount) : heading.themeCount != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (themeCount != null ? themeCount.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "headingByIdHeading",fetch = FetchType.EAGER)
    public Collection<Topic> getTopicsById() {
        return topicsById;
    }

    public void setTopicsById(Collection<Topic> topicsById) {
        this.topicsById = topicsById;
    }
}
