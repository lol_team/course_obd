package models;

import java.sql.Timestamp;

/**
 * Created by Okadzaki on 01.03.2015.
 */
public class AuthorsMessages {

    private Integer id_author;
    private Integer id_message;
    private String date_message;
    private String nickName;
    private Long messageCount;
    private String text;

    public AuthorsMessages(Integer id_author, Integer id_message, Timestamp date_message, String nickName, Long messageCount, String text) {
        this.id_author = id_author;
        this.id_message = id_message;
        this.date_message = date_message.toString();
        this.nickName = nickName;
        this.messageCount = messageCount;
        this.text = text;

    }

    public Integer getId_author() {
        return id_author;
    }

    public void setId_author(Integer id_author) {
        this.id_author = id_author;
    }

    public Integer getId_message() {
        return id_message;
    }

    public void setId_message(Integer id_message) {
        this.id_message = id_message;
    }

    public String getDate_message() {
        return date_message;
    }

    public void setDate_message(Timestamp date_message) {
        this.date_message = date_message.toString();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(Long messageCount) {
        this.messageCount = messageCount;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
