package models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by leonid on 27.02.2015.
 */
@Entity
public class Message implements Comparable<Message>{
    private Integer id;
    private String text;
    private Timestamp date;
    private Topic topicByIdTopic;
    private Users usersByIdUser;

    @Id
    @Column(name = "id")
    @SequenceGenerator(name="message_id_gen", sequenceName="message_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE ,generator="message_id_gen")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "date")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (date != null ? !date.equals(message.date) : message.date != null) return false;
        if (id != null ? !id.equals(message.id) : message.id != null) return false;
        if (text != null ? !text.equals(message.text) : message.text != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_topic", referencedColumnName = "id", nullable = false)
    public Topic getTopicByIdTopic() {
        return topicByIdTopic;
    }

    public void setTopicByIdTopic(Topic topicByIdTopic) {
        this.topicByIdTopic = topicByIdTopic;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user", referencedColumnName = "id", nullable = false)
    public Users getUsersByIdUser() {
        return usersByIdUser;
    }

    public void setUsersByIdUser(Users usersByIdUser) {
        this.usersByIdUser = usersByIdUser;
    }

    @Override
    public int compareTo(Message o) {
        return this.date.compareTo(o.getDate());
    }

}
