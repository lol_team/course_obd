package db_Proccesing;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.AuthorsMessages;
import models.Topic;
import models.UncensoredWords;
import models.Users;

/**
 * Created by Okadzaki on 28.02.2015.
 */
public class Message {


    static public List<AuthorsMessages> getMessageList(String id) {
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<AuthorsMessages> result = new ArrayList<AuthorsMessages>();
        try{
            Topic topic = entityManager.find(Topic.class,Integer.parseInt(id));
            List<models.Message> res_messages = (List<models.Message>)topic.getMessagesById();


            for (models.Message message: res_messages){

                result.add(new AuthorsMessages(message.getUsersByIdUser().getId(),
                        message.getId(),message.getDate(),
                        message.getUsersByIdUser().getLogin(),
                        message.getUsersByIdUser().getMessageCount(),
                        message.getText()));
            }
            entityManager.getTransaction().commit();

        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }    entityManagerFactory.close();
        return  result;
    }



     static public boolean addMessage(models.Message msg, String login,String id_theme){
         //LT was here
            if (checkMessage(msg))
                return false;

         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager =  entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            Query query = entityManager.createQuery("SELECT t FROM Topic t WHERE id = " + id_theme);
            Topic topic = (Topic) query.getSingleResult();
            query = entityManager.createQuery("SELECT u FROM Users u WHERE login = " + "'"+(String)login+"'");//строки должны быть в одинарных кавычках
            Users users = (Users) query.getSingleResult();
            msg.setTopicByIdTopic(topic);
            msg.setUsersByIdUser(users);
            entityManager.persist(msg);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }    entityManagerFactory.close();
         return true;
    }


    static public boolean checkMessage(models.Message message){

        String[] lexems = message.getText().split(" ");
        List <String> wordsList = getWords();
        for(String word: lexems)
        {
            if (wordsList.contains(word))
                return true;
        }
        return false;

    }

    static public List<String> getWords(){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<UncensoredWords> result = null;
        List<String> result_word = new ArrayList<String>();
        entityManager.getTransaction().begin();
        try{
            Query query = entityManager.createQuery("SELECT u FROM UncensoredWords u");
            result = query.getResultList();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }    entityManagerFactory.close();

        for(UncensoredWords words:result)
            result_word.add(words.getWord());
        return  result_word;
    }

    static public String getLastMsg(){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        models.Message msg = null;
        String lastMsg=null;
        try{
            javax.persistence.Query query = entityManager.createQuery("SELECT m FROM Message m ORDER BY date DESC").setMaxResults(1);
            msg = (models.Message) query.getSingleResult();
            lastMsg = msg.getText();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {entityManager.close();entityManagerFactory.close();
            return lastMsg;
        }
    }

    static public String getUserLastMsg(){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        models.Message msg = null;
        String user=null;
        try{
            javax.persistence.Query query = entityManager.createQuery("SELECT m FROM Message m ORDER BY date DESC").setMaxResults(1);
            msg = (models.Message) query.getSingleResult();
            user = msg.getUsersByIdUser().getLogin();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {entityManager.close();entityManagerFactory.close();
            return user;
        }

    }
    static public void delMsg(String id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            models.Message message = entityManager.find(models.Message.class, Integer.parseInt(id));
            entityManager.remove(message);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
    }
    static public String getTextById(String id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        String result = null;
        try{
            models.Message message = entityManager.find(models.Message.class,Integer.parseInt(id));
            result = message.getText();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
        return result;
    }

    static public boolean isTopicConsistMsg(String id_message,String id_topic){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        boolean result = false;
        try{
            Topic topic = entityManager.find(Topic.class,Integer.parseInt(id_topic));
            List<models.Message> messageList = (List<models.Message>)topic.getMessagesById();
            for (models.Message itr:messageList){
                if (itr.getId() == Integer.parseInt(id_message))
                {result = true; break;}
            }
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
        return result;
    }
    static public boolean editMessage(String text, String id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        boolean ok = false;
        try{
            models.Message message = entityManager.find(models.Message.class,Integer.parseInt(id));
                message.setText(text);
            if (!checkMessage(message)){
                entityManager.merge(message);
                ok = true;
            }
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
        return ok;
    }
    static public models.Message getLastMessage(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List <Object[]> temp = null;
        try{
            StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("last_message");
            storedProcedureQuery.execute();
            temp = storedProcedureQuery.getResultList();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
            entityManagerFactory.close();
        }
        for (Object[] object:temp){
            models.Message message = new models.Message();
            message.setId((Integer) object[0]);
            message.setText((String) object[1]);
            message.setDate((Timestamp) object[2]);
             entityManagerFactory = Persistence.createEntityManagerFactory("forum");
             entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            Topic topic = entityManager.find(Topic.class,(Integer)object[4]);
            entityManager.getTransaction().commit();
            entityManager.close();
            entityManagerFactory.close();
            message.setTopicByIdTopic(topic);
            return  message;
        }
        return null;
    }
}
