package db_Proccesing;
import javax.persistence.*;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by leonid on 27.02.2015.
 */
public class Heading {
    public Heading() {
    }
    static public List<models.Heading> getAllHeadings(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<models.Heading> result = null;
        try {
            Query query = entityManager.createQuery("SELECT h FROM Heading h ORDER BY h.name");
            result = query.getResultList();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }
        entityManagerFactory.close();
        return result;
    }

    static public void deleteHeadById(String id){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        //entityManager.createQuery("DELETE FROM heading WHERE id="+id);
        try {
            models.Heading heading = entityManager.find(models.Heading.class, Integer.parseInt(id));
            entityManager.remove(heading);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }
        entityManagerFactory.close();


    }

    static public void addHeading(String name){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager= entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            models.Heading heading = new models.Heading();
            heading.setName(name);
            heading.setThemeCount(0L);
            entityManager.persist(heading);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }entityManagerFactory.close();
    }

    static public String getLoginById(String id){
        String name=null;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager= entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            models.Heading heading = entityManager.find(models.Heading.class, Integer.parseInt(id));
            name = heading.getName();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {

            entityManager.close();

        }    entityManagerFactory.close();
        return name;

    }

    static public void renameHeadById(String name, String id){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            models.Heading heading = entityManager.find(models.Heading.class, Integer.parseInt(id));
            heading.setName(name);
            entityManager.merge(heading);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }entityManagerFactory.close();

    }
    static  public Long getCountTopic(int id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            StoredProcedureQuery storedProcedureQuery= entityManager.createStoredProcedureQuery("cnt_of_topic");
            storedProcedureQuery.setParameter(id,1);
            storedProcedureQuery.execute();
            BigInteger bigInteger = (BigInteger)storedProcedureQuery.getSingleResult();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }entityManagerFactory.close();

        return  null;
    }

}
