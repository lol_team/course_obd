package db_Proccesing;

import models.*;
import models.Heading;
import models.Message;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by leonid on 27.02.2015.
 */
public class Topic {


    static public boolean addTopic(models.Topic topic, String id_heading,String msg, String login){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        boolean Add = false;
        entityManager.getTransaction().begin();
        try{
            Query query = entityManager.createQuery("SELECT h FROM Heading h WHERE id = " + id_heading);
            Heading heading = (Heading) query.getSingleResult();
            query = entityManager.createQuery("SELECT u FROM Users u WHERE login = " + "'"+login+"'");
            Users user = (Users) query.getSingleResult();
            topic.setHeadingByIdHeading(heading);
            Message message = new Message();
            message.setText(msg);
            message.setDate(topic.getDateCreated());

            message.setTopicByIdTopic(topic);
            message.setUsersByIdUser(user);

            entityManager.persist(topic);
            entityManager.persist(message);
            entityManager.getTransaction().commit();
            Add=true;
        }catch (Exception e){
            entityManager.getTransaction().rollback();
            Add=false;
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
        return Add;
    }
static public String getTopicIdByTitle(String title){
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

    EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.getTransaction().begin();
    int id = 0;
    try{
         id = (Integer)entityManager.createQuery("SELECT id FROM Topic WHERE name = " + "'" + title + "'").getSingleResult();
        entityManager.getTransaction().commit();
    }catch (Exception e){
        entityManager.getTransaction().rollback();
    }finally {
        entityManager.close();entityManagerFactory.close();
    }
    return Integer.toString(id);
}


    static public List<models.Topic> getAllTopics(String id){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<models.Topic> result = null;
        try {
            Heading heading = entityManager.find(Heading.class,Integer.parseInt(id));
            result = (List<models.Topic>) heading.getTopicsById();
            Set setItems = new LinkedHashSet(result);
            result.clear();
            result.addAll(setItems);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
        return result;
    }


   static public void addTopic(models.Topic topic, String id_heading){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        try{
            Query query = entityManager.createQuery("SELECT h FROM Heading h WHERE id = " + id_heading);
            Heading heading = (Heading) query.getSingleResult();
            topic.setHeadingByIdHeading(heading);
            entityManager.persist(topic);
            entityManager.getTransaction().commit();

        }catch (Exception e){
            entityManager.getTransaction().rollback();

        }finally {
            entityManager.close();entityManagerFactory.close();
        }
    }
   static public void delTopic(String id){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            models.Topic topic = entityManager.find(models.Topic.class,Integer.parseInt(id));
            entityManager.remove(topic);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
    }
    static public boolean getStatus(String id){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        models.Topic topic = null;
        entityManager.getTransaction().begin();
        try {
            topic  = entityManager.find(models.Topic.class, Integer.parseInt(id));
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
        return topic.getFlag();

    }

    static public String getLastTopic(){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        models.Topic top = null;
        String lastTop=null;
        try{
            javax.persistence.Query query = entityManager.createQuery("SELECT t FROM Topic t ORDER BY dateCreated DESC").setMaxResults(1);
            top = (models.Topic) query.getSingleResult();
            lastTop = top.getName();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
            return lastTop;
        }
    }
    static public void replaceTopic(String new_heading, String id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            models.Topic topic = entityManager.find(models.Topic.class,Integer.parseInt(id));
            Heading heading = entityManager.find(Heading.class,Integer.parseInt(new_heading));
            topic.setHeadingByIdHeading(heading);
            entityManager.merge(topic);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
    }
    static public void renameTopic(String id, String name){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            models.Topic topic = entityManager.find(models.Topic.class,Integer.parseInt(id));
            topic.setName(name);
            entityManager.merge(topic);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
    }

    static public String getIdHeading(String id_topic){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        String id_heading = null;
        try{
            models.Topic topic = entityManager.find(models.Topic.class,Integer.parseInt(id_topic));
            id_heading = String.valueOf(topic.getHeadingByIdHeading().getId());
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
            return id_heading;
        }
    }

    static public String getNameHeading(String id_topic){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        String id_heading = null;
        String name = null;
        try{
            models.Topic topic = entityManager.find(models.Topic.class,Integer.parseInt(id_topic));
            id_heading = String.valueOf(topic.getHeadingByIdHeading().getId());
            models.Heading heading = entityManager.find(models.Heading.class,Integer.parseInt(id_heading));
            name = heading.getName();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
            return name;
        }
    }

    static public String getTopicName(String id_topic){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        String name = null;
        try{
            models.Topic topic = entityManager.find(models.Topic.class,Integer.parseInt(id_topic));;
            name = topic.getName();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
            return name;
        }
    }
    static public void setFlag (String id,String flag){

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            models.Topic topic = entityManager.find(models.Topic.class,Integer.parseInt(id));
            if (flag.equals("0"))
            topic.setFlag(false);
            else if (flag.equals("1"))
                topic.setFlag(true);

            entityManager.merge(topic);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();

        }finally {
            entityManager.close();entityManagerFactory.close();
        }
    }
}
