package db_Proccesing;

import models.Users;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.security.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

/**
 * Created by Okadzaki on 01.03.2015.
 */
public class Users_processing {


    static public boolean addUser(String login, String password) {
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        boolean add = false;
        try {
            Users users = new Users();
            users.setLogin(login);
            users.setPassword(password);
            users.setDateCreated(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
            users.setRole(0);
            users.setMessageCount(0L);
            entityManager.persist(users);

            entityManager.getTransaction().commit();
            add = true;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            add = false;
        } finally {
            entityManager.close();
            entityManagerFactory.close();
        }
        return add;
    }

    static public boolean addUser(String login, String password,String role) {
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        boolean add = false;
        try {
            Users users = new Users();
            users.setLogin(login);
            users.setPassword(password);
            users.setDateCreated(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
            users.setRole(Integer.parseInt(role));
            users.setMessageCount(0L);
            entityManager.persist(users);
            entityManager.getTransaction().commit();
            add = true;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            add = false;
        } finally {
            entityManager.close();            entityManagerFactory.close();

        }
        return add;
    }

    //Удаление пользователя из БД (для админа будет только).
    static public boolean delUser(String id,HttpServletRequest request,HttpServletResponse response) {
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        boolean del = false;
        try {
            Users user = entityManager.find(Users.class, Integer.parseInt(id));
            String login = Users_processing.getLoginFromCookies(request);
            if (user.getLogin().equals(login))
                Users_processing.deleteCookie(response,request);
            entityManager.remove(user);
            entityManager.getTransaction().commit();
            del = true;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
            entityManagerFactory.close();

        }
        return del;
    }


    static public boolean isExistUser(String login, String password) {
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        boolean result = false;
        try {
            //  result = entityManager.createQuery("select exists (select true from Users where login="+ login + " AND password = " + password + ")",Boolean.class).getSingleResult();
            javax.persistence.Query query = entityManager.createQuery("SELECT u FROM Users u");
            List<Users> users = query.getResultList();
            for (Users user : users) {
                if (user.getLogin().equals(login) && user.getPassword().equals(password))
                    result = true;
            }
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            result = false;
        } finally {
            entityManager.close();            entityManagerFactory.close();

        }
        return result;
    }

    static public boolean isExistUser(String login) {
        if (login == null)
            return false;
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        boolean result = false;
        try {
            //  result = entityManager.createQuery("select exists (select true from Users where login="+ login + " AND password = " + password + ")",Boolean.class).getSingleResult();
            javax.persistence.Query query = entityManager.createQuery("SELECT u FROM Users u");
            List<Users> users = query.getResultList();
            for (Users user : users) {
                if (user.getLogin().equals(login))
                    result = true;
            }
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            result = false;
        } finally {
            entityManager.close();
            entityManagerFactory.close();

        }
        return result;
    }


    static public boolean isUserAuth(HttpServletRequest request) {
        Cookie cookies[] = request.getCookies();
        Cookie myCookie = null;
        if (cookies != null)

            for (int i = 0; i < cookies.length; i++)
                if (cookies[i].getName().equals("forum_login"))
                    return true;

        return false;
    }
    static public boolean isUserAdmin(String login){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        int role = 0;
        try{
            javax.persistence.Query query = entityManager.createQuery("SELECT role FROM Users WHERE login = " + "'" + login + "'");
            role = (Integer)query.getSingleResult();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
            entityManagerFactory.close();

        }
        if (role >0)
            return true;
        else
            return false;

    }

    static public String getLoginFromCookies(HttpServletRequest request) {
        String login = null;
        if (!isUserAuth(request))
            return login;
        Cookie cookies[] = request.getCookies();

        if (cookies != null)
            for (int i = 0; i < cookies.length; i++)
                if (cookies[i].getName().equals("forum_login"))
                    return cookies[i].getValue();
        return login;
    }


    static public List<Users> getAllUsers() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<Users> users = null;
        try {
            users = entityManager.createQuery("from Users").getResultList();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
            entityManagerFactory.close();

        }
        return users;
    }

    static public boolean valid_user(String login) {
        int message_count = 0;
        String id = getIdByLogin(login);
        message_count = getUserMessageCount(id);
        if (message_count > 10)
            return true;
        else
            return false;
    }


    static public void deleteCookie(HttpServletResponse response, HttpServletRequest request) {
        Cookie cookies[] = request.getCookies();
        Cookie delCookie = null;
        for (int i = 0; i < cookies.length; i++)
            if (cookies[i].getName().equals("forum_login"))
                delCookie = cookies[i];
        delCookie.setMaxAge(0);
        delCookie.setValue(null);
        response.addCookie(delCookie);
    }


    static public Users getUserByLogin(String login) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Users users = null;
        try {
            javax.persistence.Query query = entityManager.createQuery("SELECT u from Users u WHERE login = " + "'" + login + "'");
            users = (Users) query.getSingleResult();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
            entityManagerFactory.close();
        }
        return users;
    }


    static public String getLoginById(String id){
        //LT was here
        String Result=null;
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            models.Users user = entityManager.find(models.Users.class, Integer.parseInt(id));
            Result = user.getLogin();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();

        }finally {
            entityManager.close();
            entityManagerFactory.close();

            return Result;
        }

    }

    static public void changeUserById(String name,String password,String role, String id){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            models.Users user = entityManager.find(models.Users.class, Integer.parseInt(id));
            user.setLogin(name);
            user.setRole(Integer.parseInt(role));
            user.setPassword(password);
            entityManager.merge(user);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
            entityManagerFactory.close();

        }

    }

    static public String getUsersCount(){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        String count=null;
        try{
            javax.persistence.Query query = entityManager.createQuery("SELECT COUNT(*) FROM Users").setMaxResults(1);
            count = query.getSingleResult().toString();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {entityManager.close();            entityManagerFactory.close();

            return count;
        }

    }

    static public List<Users> getTop(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
          List<Object[]> temp = null;
        List<Users> result = null;
        try{
           // javax.persistence.Query query= entityManager.createQuery("FROM Users ORDER BY messageCount DESC").setMaxResults(10);

            StoredProcedureQuery storedProcedureQuery= entityManager.createStoredProcedureQuery("show_top10");
            storedProcedureQuery.execute();
            temp = storedProcedureQuery.getResultList();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
            entityManagerFactory.close();
        }
        result = new ArrayList<Users>();
        for (Object[] object:temp){
            Users users = new Users();
            users.setId((Integer) object[0]);
            users.setLogin((String) object[1]);
            users.setPassword((String) object[2]);
            users.setDateCreated((java.sql.Timestamp) object[3]);
            BigInteger value = (BigInteger)object[4];
            users.setMessageCount(value.longValue());
            users.setRole((Integer) object[5]);
            result.add(users);
        }
        return result;
    }

    static public int getUserMessageCount(String id) {
        //LT was here
        int Result = -1;
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            models.Users user = entityManager.find(models.Users.class, Integer.parseInt(id));
            Result = user.getMessageCount().intValue();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();

        } finally {
            entityManager.close();            entityManagerFactory.close();

            return Result;
        }
    }

    static public String getIdByLogin(String login) {
        //LT was here
        String Result = null;
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            Result = entityManager.createQuery("SELECT id from Users where login=" + "'" + login + "'").getSingleResult().toString();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();

        } finally {
            entityManager.close();
            entityManagerFactory.close();

            return Result;
        }
    }
    static public Users getLastUser(){
        Users user = null;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<Object[]> temp = null;

        try{
            StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("last_user");
            storedProcedureQuery.execute();
            temp = storedProcedureQuery.getResultList();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
            entityManagerFactory.close();
        }
        for (Object[] object:temp){
            user = new Users();
            user.setId((Integer) object[0]);
            user.setLogin((String) object[1]);
            user.setPassword((String) object[2]);
            user.setDateCreated((java.sql.Timestamp) object[3]);
            BigInteger value = (BigInteger)object[4];
            user.setMessageCount(value.longValue());
            user.setRole((Integer) object[5]);
            return user;
        }
        return null;
    }
}