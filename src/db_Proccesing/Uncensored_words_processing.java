package db_Proccesing;

import models.UncensoredWords;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by Okadzaki on 20.03.2015.
 */
public class Uncensored_words_processing {

    static public List<UncensoredWords> getlAllWords(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<UncensoredWords> result = null;
        try {
            result = entityManager.createQuery("from UncensoredWords").getResultList();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
        return result;
    }

    static public void addWord(String word){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            UncensoredWords words = new UncensoredWords();
            words.setWord(word);
            entityManager.persist(words);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
    }
    static public void delWord(String id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            UncensoredWords uncensoredWords = entityManager.find(UncensoredWords.class,Integer.parseInt(id));
            entityManager.remove(uncensoredWords);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
    }
    static public String getWordById(String id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        String result = null;
        try{
            UncensoredWords uncensoredWords = entityManager.find(UncensoredWords.class,Integer.parseInt(id));
            result = uncensoredWords.getWord();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }
        return result;
    }

    static public void renameWordById(String name, String id){
         EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            models.UncensoredWords word = entityManager.find(models.UncensoredWords.class, Integer.parseInt(id));
            word.setWord(name);
            entityManager.merge(word);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();entityManagerFactory.close();
        }

    }
}
