package controller;

import db_Proccesing.*;
import models.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * Created by Okadzaki on 20.03.2015.
 */
@WebServlet(name = "adminContoller")
public class adminController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        if (!Users_processing.isUserAuth(request) && !Users_processing.isUserAdmin(Users_processing.getLoginFromCookies(request))){
            response.getWriter().println("Hacking attempt!");
            return;
        }

        if (request.getParameter("topic_id") != null){
            String message = request.getParameter("message");
            String id_message = request.getParameter("message_id");
            String topic_id = request.getParameter("topic_id");
            boolean check = Message.editMessage(message,id_message);
            if (check){
                response.sendRedirect("/showtopic?id="+topic_id);
                return;
            }else {
                response.getWriter().println("Замечено редактирование на нецензурное слово..ну или какая-то ошибка случилась. Не знаю короч");
                response.getWriter().println("<a onclick=\"history.back();\">Назад</a>");
                return;
            }
        }
        String url = request.getParameter("currentPage");
        Map<String, String[]> map = request.getParameterMap();
        Map<String, List<String>> from_URL = Urls.getQueryParams(url);
        String from = from_URL.get("show").get(0);
        if (from.equals("headings_control")) {
            String name = map.get("name")[0];
            Heading.addHeading(name);
            response.sendRedirect("/admin?show=headings_control");
            return;
        } else if (from.equals("filter")) {
            String word = map.get("word")[0];
            Uncensored_words_processing.addWord(word);
            response.sendRedirect("/admin?show=filter");
            return;
        } else if (from.equals("users_control")) {
            String login = map.get("login")[0];
            String role = map.get("role")[0];
            String password = map.get("password")[0];
            if (Users_processing.isExistUser(login)) {
                PrintWriter printWriter = response.getWriter();
                printWriter.println("Ошибка при добавлении пользователя!");
                printWriter.println("<a onclick=\"history.back();\">Назад</a>");
                return;
            }
            boolean ok = Users_processing.addUser(login, password,role);//Nado proverka
            if (ok == false) {
                PrintWriter printWriter = response.getWriter();
                printWriter.println("Ошибка при добавлении пользователя!");
                printWriter.println("<a onclick=\"history.back();\">Назад</a>");
                return;
            }
            response.sendRedirect("/admin?show=users_control");
            return;
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Users users = Users_processing.getUserByLogin(Users_processing.getLoginFromCookies(request));
        if (users == null || users.getRole() <= 0) {
            request.getRequestDispatcher("/404.jsp").forward(request, response);
            return;
        }
        String doing = request.getParameter("show");
        if (doing == null)
            request.getRequestDispatcher("/WEB-INF/views/admin/admin-panel-general.jsp").forward(request, response);

        else if (doing.equals("users_control")) {
            String action = request.getParameter("do");
            if (action != null) {
                if (action.equals("edit")) {
                    String id = request.getParameter("id");
                    String login = Users_processing.getLoginById(id);
                    String password = Users_processing.getUserByLogin(login).getPassword();
                    getServletContext().setAttribute("id_user",id);
                    getServletContext().setAttribute("login",login);
                    getServletContext().setAttribute("password",password);

                    request.getRequestDispatcher("/WEB-INF/views/admin/edit-user.jsp").forward(request, response);
                } else if (action.equals("delete")) {
                    String id = request.getParameter("id");
                    boolean isOk = Users_processing.delUser(id,request,response);
                }
            }
            getServletContext().setAttribute("users", Users_processing.getAllUsers());
            request.getRequestDispatcher("/WEB-INF/views/admin/admin-panel-users.jsp").forward(request, response);

        } else if (doing.equals("headings_control")) {
            String action = request.getParameter("do");
            if (action != null) {
                if (action.equals("edit")) {
                    String id = request.getParameter("id");
                    getServletContext().setAttribute("id_heading",id);
                    request.getRequestDispatcher("/WEB-INF/views/admin/edit-rubric.jsp").forward(request, response);
                } else if (action.equals("delete")) {
                    String id = request.getParameter("id");
                    Heading.deleteHeadById(id);
                }
            }
            getServletContext().setAttribute("headings", Heading.getAllHeadings());
            request.getRequestDispatcher("/WEB-INF/views/admin/admin-panel-rubric.jsp").forward(request, response);

        } else if (doing.equals("filter")) {
            String action = request.getParameter("do");
            if (action != null) {
                if (action.equals("edit")) {
                    String id = request.getParameter("id");
                    getServletContext().setAttribute("id_word",id);
                    getServletContext().setAttribute("word", Uncensored_words_processing.getWordById(request.getParameter("id")));
                    request.getRequestDispatcher("/WEB-INF/views/admin/edit-word.jsp").forward(request, response);
                } else if (action.equals("delete")) {
                    String id = request.getParameter("id");
                    Uncensored_words_processing.delWord(id);
                }
            }
            getServletContext().setAttribute("words", Uncensored_words_processing.getlAllWords());
            request.getRequestDispatcher("/WEB-INF/views/admin/admin-panel-words.jsp").forward(request, response);
        } else if (doing.equals("message")) {
            String action = request.getParameter("do");
            if (action != null) {
                if (action.equals("edit")) {
                    response.setContentType("text/html; charset=UTF-8");
                    request.setCharacterEncoding("UTF-8");
                    String msg = Message.getTextById(request.getParameter("id"));
                    String topic_id = request.getParameter("topic");
                    boolean ok = Message.isTopicConsistMsg(request.getParameter("id"), topic_id);
                    if (ok) {
                        getServletContext().setAttribute("message",msg);
                        getServletContext().setAttribute("id_message",request.getParameter("id"));
                        getServletContext().setAttribute("topic_id",topic_id);
                        request.getRequestDispatcher("/WEB-INF/views/admin/edit-message.jsp").forward(request, response);
                    } else {
                        PrintWriter printWriter = response.getWriter();
                        printWriter.println("Hacking attempt!");
                        return;
                    }
                } else if (action.equals("delete")) {
                    String id = request.getParameter("id");
                    String topic_id = request.getParameter("topic");
                    boolean ok = Message.isTopicConsistMsg(id, topic_id);
                    if (ok) {
                        Message.delMsg(id);
                        response.sendRedirect("/showtopic?id=" + topic_id);
                    } else {
                        PrintWriter printWriter = response.getWriter();
                        printWriter.println("Hacking attempt!");
                        return;
                    }
                }
            }
        }


    }
}
