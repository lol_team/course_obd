package controller;

import db_Proccesing.Users_processing;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Okadzaki on 24.03.2015.
 */
@WebServlet(name = "topViewer")
public class topViewer extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServletContext().setAttribute("users", Users_processing.getTop());
        request.getRequestDispatcher("/WEB-INF/views/top10.jsp").forward(request,response);

    }
}
