package controller;

import db_Proccesing.Heading;
import db_Proccesing.Uncensored_words_processing;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by leonid on 23.03.2015.
 */
@WebServlet(name = "editWordServlet")
public class editWordServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String id = request.getParameter("currentPage");
        String name = request.getParameter("edit_word");
        Uncensored_words_processing.renameWordById(name, id);
        response.sendRedirect("/admin?show=filter");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
