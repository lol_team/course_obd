package controller;

import db_Proccesing.Topic;
import db_Proccesing.Users_processing;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Created by leonid on 27.02.2015.
 */
@WebServlet(name = "showForumServlet")
public class showForumServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        if (!Users_processing.isUserAuth(request))
            request.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(request,response);

        String id = null;
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()){
            String param = params.nextElement();
            id = "id".equals(param)?request.getParameter(param):id;
        }
        try{
            String name_heading =  db_Proccesing.Heading.getLoginById(id);
            getServletContext().setAttribute("name_heading", name_heading);
            getServletContext().setAttribute("topics", Topic.getAllTopics(id));
            getServletContext().setAttribute("id_heading", id);
            request.getRequestDispatcher("/WEB-INF/views/showforum.jsp").forward(request,response);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
