package controller;

import db_Proccesing.Message;
import db_Proccesing.Topic;
import db_Proccesing.Users_processing;
import models.AuthorsMessages;
import models.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by Okadzaki on 28.02.2015.
 */
@WebServlet(name = "showTopicServlet")
public class showTopicServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!Users_processing.isUserAuth(request))
            request.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(request,response);
        String id = null;
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()){
            String param = params.nextElement();
            id = "id".equals(param)?request.getParameter(param):id;
        }
        try{
            List<AuthorsMessages> messages = Message.getMessageList(id);
            boolean status = Topic.getStatus(id);
            String id_heading = Topic.getIdHeading(id);
            String name_topics_heading = Topic.getNameHeading(id);
            String name_topic = Topic.getTopicName(id);
            getServletContext().setAttribute("id_topics_heading",id_heading);
            getServletContext().setAttribute("name_topics_heading",name_topics_heading);
            getServletContext().setAttribute("status",status);
            getServletContext().setAttribute("current_topic",id);
            getServletContext().setAttribute("name_topic",name_topic);
            getServletContext().setAttribute("messages", messages);
            request.getRequestDispatcher("/WEB-INF/views/showtopic.jsp").forward(request,response);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
