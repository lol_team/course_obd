package controller;

import db_Proccesing.Users_processing;
import models.Topic;
import models.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Okadzaki on 24.03.2015.
 */
@WebServlet(name = "topicController")
public class topicController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        Users users = Users_processing.getUserByLogin(Users_processing.getLoginFromCookies(request));
        if (users == null || users.getRole() <= 0) {
            request.getRequestDispatcher("/404.jsp").forward(request, response);
            return;
        }
        if (request.getParameter("to_heading") !=null){
            String new_heading = request.getParameter("to_heading");
            String id = request.getParameter("topic_id");
            db_Proccesing.Topic.replaceTopic(new_heading, id);
            response.sendRedirect("/showtopic?id="+id);
            return;
        }
        else if(request.getParameter("delete") !=null){
            String id = request.getParameter("delete");
            db_Proccesing.Topic.delTopic(id);
            response.sendRedirect("/");
            return;
        }
        else if(request.getParameter("flag") !=null){
            String id = request.getParameter("flag");
            String flag = request.getParameter("id");
            db_Proccesing.Topic.setFlag(id,flag);
            response.sendRedirect("/showtopic?id="+id);
            return;
        }
        else {
            String name = request.getParameter("new_name");
            db_Proccesing.Topic.renameTopic(request.getParameter("topic_id"),name);
            response.sendRedirect("/showtopic?id="+request.getParameter("topic_id"));
            return;
        }

    }
}
