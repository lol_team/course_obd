package controller;

import db_Proccesing.Users_processing;
import models.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Otani on 03.03.2015.
 */
@WebServlet(name = "registerProcessingServlet")
public class registerProcessingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String confirm = request.getParameter("confirm");
        PrintWriter printWriter = response.getWriter();
        if (confirm.equals(password)){
            if (Users_processing.isExistUser(login))
            {
                printWriter.println("Регистрация не удалась. Возможно такой пользователь уже существует");
                return;
            }
            Users_processing users_processing = new Users_processing();
                      boolean res=   users_processing.addUser(login,password);
                if (!res){
                    printWriter.println("Регистрация не удалась. Возможно такой пользователь уже существует");
                    printWriter.println("<a href = \"#\" onclick=\"history.back();\">Назад</a>");
                    return;
                }
            else{
                    printWriter = response.getWriter();
                    printWriter.println("Вы успешно зарегистрировались!");
                    printWriter.println("<a href = \"/\"> Перейти на главную страницу</a>");
                    return;
                }

        }else {
            printWriter.println("Пароли не совпадают!");
            printWriter.println("<a href = \"#\" onclick=\"history.back();\">Назад</a>");
            return;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
