package controller;

import db_Proccesing.Heading;
import db_Proccesing.Users_processing;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by leonid on 21.03.2015.
 */
@WebServlet(name = "editUserServlet")
public class editUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String id = request.getParameter("currentPage");
        String name = request.getParameter("edit_user");
        String password = request.getParameter("password");
        String role = request.getParameter("role");
        Users_processing.changeUserById(name,password,role,id);
        response.sendRedirect("/admin?show=users_control");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
