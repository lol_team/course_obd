package controller;

import db_Proccesing.Users_processing;
import models.Topic;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

/**
 * Created by Otani on 11.03.2015.
 */
@WebServlet(name = "addTopicServlet")
public class addTopicServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        if (!Users_processing.isUserAuth(request) || !Users_processing.isExistUser(Users_processing.getLoginFromCookies(request)))
            response.sendRedirect("/");
        String id_heading = request.getParameter("id_heading");
        String msg = request.getParameter("text_msg");

        String login = Users_processing.getLoginFromCookies(request);
        Topic topic = new Topic();
        topic.setFlag(true);
        topic.setMessageCount(0L);
        topic.setDateCreated((new java.sql.Timestamp(Calendar.getInstance().getTime().getTime())));
        if (!request.getParameter("title_topic").equals("") && !request.getParameter("title_topic").equals(" ") && request.getParameter("title_topic")!=null){
            topic.setName(request.getParameter("title_topic"));

            boolean check = db_Proccesing.Topic.addTopic(topic,id_heading,msg,login);


            if (check) {
                String id = db_Proccesing.Topic.getTopicIdByTitle(topic.getName());
                response.sendRedirect("/showtopic?id="+id);
                return;
            }//Добавлено, ок}
            else {
                PrintWriter printWriter = response.getWriter();
                printWriter.println("Возникла ошибка при создании темы");
                printWriter.println("<a href = \"#\" onclick=\"history.back();\">Назад</a>");
                return;
            }
           }
        else{
            PrintWriter printWriter = response.getWriter();
            printWriter.println("Возникла ошибка при создании темы");
            printWriter.println("<a href = \"#\" onclick=\"history.back();\">Назад</a>");
            return;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id_heading = request.getParameter("id_heading");
        getServletContext().setAttribute("id_heading",id_heading);
        request.getRequestDispatcher("/WEB-INF/views/addtopic.jsp").forward(request,response);

    }
}
