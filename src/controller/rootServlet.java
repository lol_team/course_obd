package controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import db_Proccesing.*;
import db_Proccesing.Heading;
import db_Proccesing.Message;
import models.*;

/**
 * Created by leonid on 27.02.2015.
 */
@WebServlet(name = "rootServlet")
public class rootServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (Users_processing.isUserAuth(request)) {
            String text = Message.getLastMessage().getText().substring(0,15)+"...";

            getServletContext().setAttribute("last_login",Users_processing.getLastUser().getLogin());
            getServletContext().setAttribute("last_message_text",text);
            getServletContext().setAttribute("users_count",Users_processing.getUsersCount());
            getServletContext().setAttribute("last_message_topic",Message.getLastMessage().getTopicByIdTopic().getId());
            getServletContext().setAttribute("headings", Heading.getAllHeadings());
            request.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(request, response);
        }else
        request.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(request,response);


    }
}
