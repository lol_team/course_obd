package controller;

import db_Proccesing.Message;
import db_Proccesing.Users_processing;
import org.omg.CORBA.NameValuePair;

import javax.jws.soap.SOAPBinding;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Okadzaki on 03.03.2015.
 */
@WebServlet(name = "addMessageServlet")
public class addMessageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!Users_processing.isUserAuth(request) || !Users_processing.isExistUser(Users_processing.getLoginFromCookies(request)))
            request.getRequestDispatcher("/WEB-INF/views/error.jsp").forward(request,response);
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        //LT was here
        String text_message = request.getParameter("message_txt");
        models.Message msg = new models.Message();
        msg.setText(text_message);


        String login = Users_processing.getLoginFromCookies(request);
        //получаем логин пользователя из куков



        String id_theme = request.getParameter("currentPage");
        //получаем id темы

        java.util.Date date= new java.util.Date();
        Timestamp time_current = new Timestamp(date.getTime());
        msg.setDate(time_current);
        /*устанавливаем дату отправки сообщения вручную, поскольку какого-то черта по дефолту это поле заполняться
        не хочет и сообщение не добавляеться в бд*/

        boolean ok = Message.addMessage(msg,login,id_theme);
        if (!ok) {
            PrintWriter out = new PrintWriter(
                    new OutputStreamWriter(response.getOutputStream(), "UTF8"), true);
            out.println("Вы употребили нецензурную речь в сообщении, поэтому ваше сообщение не было добавлено.");
            return;
        }
        //NEEDS HERE
        response.sendRedirect("/showtopic?id="+id_theme);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
