package controller;

import db_Proccesing.Users_processing;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Okadzaki on 03.03.2015.
 */
@WebServlet(name = "authServlet")
public class authServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        boolean isExist = Users_processing.isExistUser(login,password);
        if (isExist){

        Cookie cookie_login = new Cookie("forum_login",login);
        cookie_login.setMaxAge(24*60*60);
        response.addCookie(cookie_login);
            response.sendRedirect("/");

        }
        else {
         response.sendRedirect("/");
    //NEEDS HERE
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
