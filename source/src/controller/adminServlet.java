package controller;

import db_Proccesing.Users_processing;
import models.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Otani on 18.03.2015.
 */
@WebServlet(name = "adminServlet")
public class adminServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (Users_processing.isUserAuth(request)){
            String login = Users_processing.getLoginFromCookies(request);
            Users user = Users_processing.isExistUser(login);
            if (user.getRole()>1)
            request.getRequestDispatcher("/WEB-INF/views/admin/admin-panel-general.jsp").forward(request,response);
            else
                response.sendRedirect("/");
        }
        else
            response.sendRedirect("/");
    }
}
