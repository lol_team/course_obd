package controller;

import db_Proccesing.Users_processing;
import models.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Otani on 03.03.2015.
 */
@WebServlet(name = "registerProcessingServlet")
public class registerProcessingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String confirm = request.getParameter("confirm");
        PrintWriter printWriter = response.getWriter();
        if (confirm.equals(password)){
            Users_processing users_processing = new Users_processing();
                        users_processing.addUser(login,password);
                if (!users_processing.isAdd()){
                    printWriter.println("Регистрация не удалась. Возможно такой пользователь уже существует");
                }
            else{
                    printWriter = response.getWriter();
                    printWriter.println("Вы успешно зарегистрировались!");
                }

        }else {
            printWriter.println("Пароли не совпадают!");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
