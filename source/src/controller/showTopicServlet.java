package controller;

import db_Proccesing.Message;
import db_Proccesing.Topic;
import models.AuthorsMessages;
import models.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by Okadzaki on 28.02.2015.
 */
@WebServlet(name = "showTopicServlet")
public class showTopicServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = null;
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()){
            String param = params.nextElement();
            id = "id".equals(param)?request.getParameter(param):id;
        }
        try{

           List<AuthorsMessages> messages = Message.getMessageList(id);
            boolean status = Topic.getStatus(id);
            getServletContext().setAttribute("status",status);
           getServletContext().setAttribute("messages", messages);
           request.getRequestDispatcher("/WEB-INF/views/showtopic.jsp").forward(request,response);
        }catch (Exception e){
        e.printStackTrace();
        }
    }
}
