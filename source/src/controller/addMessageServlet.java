package controller;

import db_Proccesing.Users_processing;
import org.omg.CORBA.NameValuePair;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Okadzaki on 03.03.2015.
 */
@WebServlet(name = "addMessageServlet")
public class addMessageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //LT was here
        String text_message = request.getParameter("message_txt");
        models.Message msg = new models.Message();
        msg.setText(text_message);
        //Создаем обект модели таблицы "сообщение" для передачи текста сообщения в функцию отправки сообщения

        //String id = request.getParameter("currentPage").replaceAll("^.*?(?:\\?|&)id=(\\d+)(?:&|$).*$", "$1");
     /*   String cookieName = "forum_login";
        Cookie cookies [] = request.getCookies ();
        Cookie myCookie = null;
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals (cookieName)) {
                    myCookie = cookies[i]; break;
                }
            }
        }*/

        String login = Users_processing.getLoginFromCookies(request);
        //получаем логин пользователя из куков



        String id_theme = request.getParameter("currentPage");
        String replace_it = "http://localhost:8080/WEB-INF/views/showtopic.jsp?id=";
        id_theme = id_theme.replace(replace_it, "");
        //получаем id темы

        java.util.Date date= new java.util.Date();
        Timestamp time_current = new Timestamp(date.getTime());
        msg.setDate(time_current);
        /*устанавливаем дату отправки сообщения вручную, поскольку какого-то черта по дефолту это поле заполняться
        не хочет и сообщение не добавляеться в бд*/

        db_Proccesing.Message message = new db_Proccesing.Message();
        message.addMessage(msg,login,id_theme);

        replace_it = "WEB-INF/views/";
        response.sendRedirect(request.getParameter("currentPage").replace(replace_it,"").replace(".jsp",""));
        //передаем сообщение, логин пользователя и id темы методу добавления сообщения
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
