package controller;

import db_Proccesing.Topic;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Created by leonid on 27.02.2015.
 */
@WebServlet(name = "showForumServlet")
public class showForumServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id = null;
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()){
            String param = params.nextElement();
            id = "id".equals(param)?request.getParameter(param):id;
        }
        try{
          getServletContext().setAttribute("topics", Topic.getAllTopics(id));
          getServletContext().setAttribute("id_heading", id);
          request.getRequestDispatcher("/WEB-INF/views/showforum.jsp").forward(request,response);

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
