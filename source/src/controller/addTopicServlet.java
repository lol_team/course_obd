package controller;

import db_Proccesing.Users_processing;
import models.Topic;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;

/**
 * Created by Otani on 11.03.2015.
 */
@WebServlet(name = "addTopicServlet")
public class addTopicServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        if (!Users_processing.isUserAuth(request))
            response.sendRedirect("/");
        String id_heading = request.getParameter("id_heading");
        String replace_it = "http://localhost:8080/WEB-INF/views/addtopic.jsp?id_heading=";
        id_heading = id_heading.replace(replace_it, "");
        //получаем id темы
            String msg = request.getParameter("text_msg");

        Cookie cookies [] = request.getCookies ();
        Cookie myCookie = null;
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals ("forum_login")) {
                    myCookie = cookies[i]; break;
                }
            }
        }
      /*  if (myCookie.getValue() == null || !Users_processing.valid_user(myCookie.getValue()))
            return;*/
        String login = myCookie.getValue();

        Topic topic = new Topic();
        topic.setFlag(true);
        topic.setMessageCount(0L);
        topic.setDateCreated((new java.sql.Timestamp(Calendar.getInstance().getTime().getTime())));
        if (!request.getParameter("title_topic").equals("") && !request.getParameter("title_topic").equals(" ") && request.getParameter("title_topic")!=null){
            topic.setName(request.getParameter("title_topic"));
            db_Proccesing.Topic ok = new db_Proccesing.Topic();
            ok.addTopic(topic,id_heading,msg,login);


            if (ok.isAdd())
               response.sendRedirect("/"); //Добавлено, ок
            else {
            //Если сообщение добавлено
            }
           }
        else{
            //Тут должно быть предупреждение
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/addtopic.jsp").forward(request,response);

    }
}
