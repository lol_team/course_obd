package controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import db_Proccesing.*;
import db_Proccesing.Heading;
import db_Proccesing.Message;
import models.*;

/**
 * Created by leonid on 27.02.2015.
 */
@WebServlet(name = "rootServlet")
public class rootServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        getServletContext().setAttribute("headings", Heading.getAllHeadings());
        request.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(request,response);

    }
}
