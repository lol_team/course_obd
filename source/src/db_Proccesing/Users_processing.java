package db_Proccesing;

import models.Users;
import org.hibernate.Query;
import org.hibernate.cfg.annotations.reflection.XMLContext;
import sun.security.util.Password;

import javax.persistence.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.annotation.XmlElement;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Okadzaki on 01.03.2015.
 */
public class Users_processing {

    private boolean Add;
    private boolean Del;

    public boolean isAdd() {
        return Add;
    }

    public boolean isDel() {
        return Del;
    }

     public void addUser(String login, String password){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            //Короче, я ебал делать адекватные ограничения целостности. Вручную вписываем
            Users users = new Users();
            users.setLogin(login);
            users.setPassword(password);
           users.setDateCreated(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
            users.setRole(0);
            users.setMessageCount(0L);
            entityManager.persist(users);

            entityManager.getTransaction().commit();
            Add = true;
        }catch (Exception e){

            entityManager.getTransaction().rollback();
            Add = false;
        }finally {
            entityManager.close();
        }
    }

    //Удаление пользователя из БД (для админа будет только).
     public void delUser(int id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            javax.persistence.Query query = entityManager.createQuery("SELECT u FROM Users u WHERE id = " + id);
            Users user = (Users) query.getSingleResult();
            entityManager.remove(user);
            entityManager.getTransaction().commit();
            Del = true;
        }catch (Exception e){
            entityManager.getTransaction().rollback();
            Del = false;
        }finally {
            entityManager.close();
        }
    }


    static public boolean isExistUser(String login,String password){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        boolean result = false;
        try{
         //  result = entityManager.createQuery("select exists (select true from Users where login="+ login + " AND password = " + password + ")",Boolean.class).getSingleResult();
            javax.persistence.Query query = entityManager.createQuery("SELECT u FROM Users u");
            List<Users> users = query.getResultList();
            for (Users user:users){
                if (user.getLogin().equals(login) && user.getPassword().equals(password))
                    result=true;
            }
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
            result=false;
        }finally {
            entityManager.close();
        }
        return result;
    }
    static public Users isExistUser(String login){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Users user_ret = null;
        try{
            //  result = entityManager.createQuery("select exists (select true from Users where login="+ login + " AND password = " + password + ")",Boolean.class).getSingleResult();
            javax.persistence.Query query = entityManager.createQuery("SELECT u FROM Users u WHERE login = " + "'" + login + "'");
            user_ret = (Users)query.getSingleResult();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }
        return user_ret;
    }






    static public boolean isUserAuth(HttpServletRequest request){
        Cookie cookies [] = request.getCookies ();
            for (int i = 0; i < cookies.length; i++)
                if (cookies[i].getName().equals ("forum_login"))
                    return true;

        return false;
    }
    static public String getLoginFromCookies(HttpServletRequest request){
       String login = null;
        if (!isUserAuth(request))
                return login;
        Cookie cookies [] = request.getCookies ();
        if (cookies != null)
            for (int i = 0; i < cookies.length; i++)
                if (cookies[i].getName().equals ("forum_login"))
                    return cookies[i].getValue();
        return login;
    }




   static public boolean valid_user(String login){
       EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
       EntityManager entityManager = entityManagerFactory.createEntityManager();
       entityManager.getTransaction().begin();
       int message_count = 0;
       try{
           javax.persistence.Query query = entityManager.createQuery("SELECT messageCount FROM Users u WHERE login = " + "'" + login + "'");
            message_count = (Integer) query.getSingleResult();
           entityManager.getTransaction().commit();
       }catch (Exception e){
           entityManager.getTransaction().rollback();
       }finally {
           entityManager.close();
       }
       if (message_count >10)
           return true;
       else
           return false;
    }


    static public void deleteCookie(HttpServletResponse response, HttpServletRequest request){
        Cookie cookies [] = request.getCookies ();
        Cookie delCookie = null;
        for (int i = 0; i < cookies.length; i++)
            if (cookies[i].getName().equals ("forum_login"))
                delCookie = cookies[i];
        delCookie.setMaxAge(0);
        delCookie.setValue(null);
        response.addCookie(delCookie);
    }
    /*
    походу оно все нахуй надо
    public String getLoginById(String id){
        //LT was here
        String Result=null;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            javax.persistence.Query query = entityManager.createQuery("SELECT u FROM Users u WHERE id = " + id);
            Users user = new Users();
            user = (Users) query.getSingleResult();
            Result = user.getLogin();
        }catch (Exception e){
            entityManager.getTransaction().rollback();

        }finally {
            entityManager.close();
        }

        return Result;
    } /*
    Функция для того чтобы получить логин пользователя по известному id. Возвращает null, если пользователь не найден
    */

    /*
    public String getIdByLogin(String login){
        //LT was here
        String Result=null;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            javax.persistence.Query query = entityManager.createQuery("SELECT u FROM Users u WHERE login = " + "'"+login+"'");
            Users user = new Users();
            user = (Users) query.getSingleResult();
            Result = String.valueOf(user.getId());
        }catch (Exception e){
            entityManager.getTransaction().rollback();

        }finally {
            entityManager.close();
        }

        return Result;
    }*/
}
