package db_Proccesing;
import javax.persistence.*;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by leonid on 27.02.2015.
 */
public class Heading {
    public Heading() {
    }
    static public List<models.Heading> getAllHeadings(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<models.Heading> result = null;
        try {
            Query query = entityManager.createQuery("SELECT h FROM Heading h ORDER BY h.name");
            result = query.getResultList();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }
        return result;
    }
}
