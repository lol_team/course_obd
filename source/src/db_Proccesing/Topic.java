package db_Proccesing;

import models.*;
import models.Heading;
import models.Message;

import javax.persistence.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by leonid on 27.02.2015.
 */
public class Topic {
    private boolean Add;
    private boolean Del;

    public void addTopic(models.Topic topic, String id_heading,String msg, String login){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            Query query = entityManager.createQuery("SELECT h FROM Heading h WHERE id = " + id_heading);
            Heading heading = (Heading) query.getSingleResult();
            query = entityManager.createQuery("SELECT u FROM Users u WHERE login = " + "'"+login+"'");
            Users user = (Users) query.getSingleResult();
            topic.setHeadingByIdHeading(heading);
            Message message = new Message();
            message.setText(msg);
            message.setDate(topic.getDateCreated());

            message.setTopicByIdTopic(topic);
            message.setUsersByIdUser(user);

            entityManager.persist(topic);
            entityManager.persist(message);
            entityManager.getTransaction().commit();
            Add=true;
        }catch (Exception e){
            entityManager.getTransaction().rollback();
            Add=false;
        }finally {
            entityManager.close();
        }
    }



    static public List<models.Topic> getAllTopics(String id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<models.Topic> result = null;
        try {
            Heading heading = entityManager.find(Heading.class,Integer.parseInt(id));
            result = (List<models.Topic>) heading.getTopicsById();
            Set setItems = new LinkedHashSet(result);
            result.clear();
            result.addAll(setItems);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }
        return result;
    }

    public boolean isAdd() {
        return Add;
    }

    public boolean isDel() {
        return Del;
    }

    public void addTopic(models.Topic topic, String id_heading){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            Query query = entityManager.createQuery("SELECT h FROM Heading h WHERE id = " + id_heading);
            Heading heading = (Heading) query.getSingleResult();
            topic.setHeadingByIdHeading(heading);
            entityManager.persist(topic);
            entityManager.getTransaction().commit();
            Add=true;
        }catch (Exception e){
            entityManager.getTransaction().rollback();
            Add=false;
        }finally {
            entityManager.close();
        }
    }
    public void delTopic(models.Topic topic){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            entityManager.remove(topic);
            entityManager.getTransaction().commit();

        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }
    }
    static public boolean getStatus(String id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        models.Topic topic = null;
        entityManager.getTransaction().begin();
        try {
            topic  = entityManager.find(models.Topic.class, Integer.parseInt(id));
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }
        return topic.getFlag();

    }
}
