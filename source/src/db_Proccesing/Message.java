package db_Proccesing;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

import models.AuthorsMessages;
import models.Topic;
import models.UncensoredWords;
import models.Users;

/**
 * Created by Okadzaki on 28.02.2015.
 */
public class Message {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager ;
    private boolean Add;
    private boolean Del;
    private boolean goodMessage;

    public Message() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("forum");;
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public boolean isGoodMessage() {
        return goodMessage;
    }

    public boolean isAdd() {
        return Add;
    }

    public boolean isDel() {
        return Del;
    }

     public List<models.Users> getUsersNick(List<models.Message> messages){
        List<models.Users> result = new ArrayList<models.Users>();
        for(models.Message message:messages)
            result.add(message.getUsersByIdUser());
        return result;
    }

    static public List<AuthorsMessages> getMessageList(String id) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<AuthorsMessages> result = new ArrayList<AuthorsMessages>();
        try{
            Topic topic = entityManager.find(Topic.class,Integer.parseInt(id));
            List<models.Message> res_messages = (List<models.Message>)topic.getMessagesById();
            for (models.Message message: res_messages){

                result.add(new AuthorsMessages(message.getUsersByIdUser().getId(),
                        message.getId(),message.getDate(),
                        message.getUsersByIdUser().getLogin(),
                        message.getUsersByIdUser().getMessageCount(),
                        message.getText()));
            }
            entityManager.getTransaction().commit();

        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }
        return  result;
    }



     public void addMessage(models.Message msg, String login,String id_theme){
         //LT was here
         if (checkMessage(msg)){
             goodMessage = false;
             return;
         }else goodMessage = true;

         entityManagerFactory = Persistence.createEntityManagerFactory("forum");
         entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try{
            Query query = entityManager.createQuery("SELECT t FROM Topic t WHERE id = " + id_theme);
            Topic topic = (Topic) query.getSingleResult();
            query = entityManager.createQuery("SELECT u FROM Users u WHERE login = " + "'"+(String)login+"'");//строки должны быть в одинарных кавычках
            Users users = (Users) query.getSingleResult();
            msg.setTopicByIdTopic(topic);
            msg.setUsersByIdUser(users);
            entityManager.persist(msg);
            entityManager.getTransaction().commit();
            Add = true;
        }catch (Exception e){
            entityManager.getTransaction().rollback();
            Add = false;
        }finally {
            entityManager.close();
        }
    }


    static public boolean checkMessage(models.Message message){

        String[] lexems = message.getText().split(" ");
        List <String> wordsList = getWords();
        for(String word: lexems)
        {
            if (wordsList.contains(word))
                return true;
        }
        return false;

    }

    static public List<String> getWords(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("forum");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<UncensoredWords> result = null;
        List<String> result_word = new ArrayList<String>();
        entityManager.getTransaction().begin();
        try{
            Query query = entityManager.createQuery("SELECT u FROM UncensoredWords u");
            result = query.getResultList();
            entityManager.getTransaction().commit();
        }catch (Exception e){
            entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
        }
        for(UncensoredWords words:result)
            result_word.add(words.getWord());
        return  result_word;
    }

}
