package models;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by leonid on 27.02.2015.
 */
@Entity
public class Topic {
    private Integer id;
    private String name;
    private Long messageCount;
    private Timestamp dateCreated;
    private Boolean flag;
    private Collection<Message> messagesById;
    private Heading headingByIdHeading;

    @Id
    @Column(name = "id")
    @SequenceGenerator(name="topic_id_gen", sequenceName="topic_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE ,generator="topic_id_gen")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "message_count")
    public Long getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(Long messageCount) {
        this.messageCount = messageCount;
    }

    @Basic
    @Column(name = "date_created")
    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Basic
    @Column(name = "flag")
    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Topic topic = (Topic) o;

        if (dateCreated != null ? !dateCreated.equals(topic.dateCreated) : topic.dateCreated != null) return false;
        if (flag != null ? !flag.equals(topic.flag) : topic.flag != null) return false;
        if (id != null ? !id.equals(topic.id) : topic.id != null) return false;
        if (messageCount != null ? !messageCount.equals(topic.messageCount) : topic.messageCount != null) return false;
        if (name != null ? !name.equals(topic.name) : topic.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (messageCount != null ? messageCount.hashCode() : 0);
        result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
        result = 31 * result + (flag != null ? flag.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "topicByIdTopic",fetch = FetchType.EAGER)
    public Collection<Message> getMessagesById() {
        return messagesById;
    }

    public void setMessagesById(Collection<Message> messagesById) {
        this.messagesById = messagesById;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_heading", referencedColumnName = "id", nullable = false)
    public Heading getHeadingByIdHeading() {
        return headingByIdHeading;
    }

    public void setHeadingByIdHeading(Heading headingByIdHeading) {
        this.headingByIdHeading = headingByIdHeading;
    }
}
