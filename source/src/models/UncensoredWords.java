package models;

import javax.persistence.*;

/**
 * Created by leonid on 27.02.2015.
 */
@Entity
@Table(name = "uncensored_words", schema = "public", catalog = "forum")
public class UncensoredWords {
    private Integer id;
    private String word;

    @Id
    @Column(name = "id")
    @SequenceGenerator(name="uncensored_id_gen", sequenceName="uncensored_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE ,generator="uncensored_id_gen")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "word")
    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UncensoredWords that = (UncensoredWords) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (word != null ? !word.equals(that.word) : that.word != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (word != null ? word.hashCode() : 0);
        return result;
    }
}
