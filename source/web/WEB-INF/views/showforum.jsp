﻿
		<div class="wrapper">
				<article>
					<table class="second-navigation">
					<tbody>
						<tr>
							<td><a href="/">Главная</a> &rarr; &nbsp;</td>
							<td>Раздел 1</td>
						</tr>
					</tbody>
					</table>
					<h4 style="float:right"><a href="addTopic?id_heading=${id_heading}" title="Добавить новую тему">Новая тема</a></h4>
					<table class="table-border">
							<tbody>

                                <c:forEach var = "topic" items = "${topics}">
                                    <tr>
                                        <td class="msg-img"><img src="image/msg-topic.png" width="32" height="32" alt="theme"></td>
                                        <td><a href="showtopic?id=${topic.id}" title="${topic.name}">${topic.name}</a></td>
                                        <td class="count">Автор ... <br> Сообщений: ${topic.messageCount}</td>
                                    </tr>
                                </c:forEach>
							</tbody>
						</table>
				</article>
	</div>