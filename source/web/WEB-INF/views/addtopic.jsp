﻿<!DOCTYPE html>
		<div class="wrapper">
				<article>
					<table class="second-navigation">
					<tbody>
						<tr>
							<td><a href="index.html">Главная</a> &rarr; &nbsp;</td>
							<td><a href="topics.html">Раздел 1</a>&rarr; &nbsp;</td>
							<td>Новая тема</td>
						</tr>
					</tbody>
					</table>
				
					<h2>Создание темы в разделе 1</h2>
				
					
				<div id="add-theme">
					<div class="main_title">
						Новая тема
					</div>
					
						<div id="add-theme-form">
						<br>
							<form name="NewTheme" action="/addTopic" method="post">
								<p style="text-align:left; padding-left:2%;">Заголовок темы <input  class="titleNewTheme" type="text" maxlength="100" name="title_topic"></p>
                                <input type="hidden" name="id_heading" value="<%=request.getRequestURL().append('?').append(request.getQueryString())%>">
                                <textarea style="width:95%; background-color:rgb(255, 255, 255);" name="text_msg"></textarea>

                                <button type="submit">Опубликовать</button>
							</form>
						</div>
						
				</div>
					<br>
				</article>
	</div>
