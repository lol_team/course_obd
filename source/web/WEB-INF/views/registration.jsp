<%--
  Created by IntelliJ IDEA.
  User: Okadzaki
  Date: 01.03.2015
  Time: 22:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="wrapper">
    <article>
        <table class="second-navigation">
            <tbody>
            <tr>
                <td><a href="index.html">Главная</a> &rarr; &nbsp;</td>
                <td>Регистрация</td>
            </tr>
            </tbody>
        </table>
        <br>
        <div class="registration">
            <div class="main_title">
                Регистрация
            </div>

            <div class="reg-form">
                <br>
                <form name="registration" action="/registration/processing" method="post">
                    <p style="text-indent:15px">Имя пользователя:  <input  class="reg" type="text" maxlength="20" name="login"></p>
                    <p  style="text-indent:89px">Пароль: <input class ="reg" type="password" name="password" maxlength="20"></p>
                    <p>Подтвердите пароль: <input class="reg" type="password" name="confirm" maxlength="20"></p>
                    <div style="margin-left:47%;">
                        <button type="submit">Зарегистрироваться</button> </div>
                </form>
            </div>

        </div>
        <br>
    </article>

</div>
