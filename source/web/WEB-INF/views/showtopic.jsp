﻿<!DOCTYPE html>

		<div class="wrapper">
				<article>
					<table class="second-navigation">
					<tbody>
						<tr>
							<td><a href="index.html">Главная</a> &rarr; &nbsp;</td>
							<td><a href="topics.html">Раздел 1</a>&rarr; &nbsp;</td>
							<td>Тема 1</td>
						</tr>
					</tbody>
					</table>
				
					<h2>Тема 1</h2>
				
					<div class="main_title" style="font-size:0.8em; font-weight:normal;">
						Сообщений в теме:
					</div>

                    <c:forEach var = "message" items = "${messages}">
                        <table class="table-border">
                            <tbody>
                            <tr>
                                <td class="topic-date" colspan="2"> Дата: ${message.date_message}</td>
                            </tr>
                            <tr>
                                <td class="topic-author">${message.nickName}</td>
                                <td>${message.text}</td>
                            </tr>

                            </tbody>
                        </table>
                    </c:forEach>
                    <c:if test = "${status == true}">
					<div class="txt-msg">
						<p>Введите сообщение:</p>
							<form  name="text-message" class="button-message" action="addMsg" method="post">
								<textarea name="message_txt"></textarea>

                        <input type="hidden" name="currentPage" value="<%=request.getRequestURL().append('?').append(request.getQueryString())%>">
						<p><button style="float:right;" type="submit">Ответить</button></p>
                                <br><br>
                            </form>
					</div>
                    </c:if>
                    <c:if test = "${status == false}">
                        <div class="txt-msg">
                            <p>Тема закрыта!</p>
                        </div>
                    </c:if>
				</article>
	</div>
