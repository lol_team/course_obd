
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="wrapper">
    <article>
        <div class="main_title">
            Форум для курсового проекта по проектированию БД
        </div>
        <table class="table-border">
            <tbody>

            <c:forEach var = "heading" items = "${headings}">
                <tr>
                    <td class="msg-img"><img src="image/message.png" width="32" height="32" alt="theme"></td>
                    <td><a href="showforum?id=${heading.id}" title=${heading.name}>${heading.name}</a></td>
                    <td class="count">${heading.themeCount} Тем <br> Сообщений:</td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
        <br>
        <div class="main_title">
            Новости и полезная информация
        </div>
        <table class="table-border">
            <tbody>

            </tbody>
        </table>
    </article>

</div>
