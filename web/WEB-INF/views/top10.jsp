<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<div class="wrapper">
    <article>
        <table class="second-navigation">
            <tbody>
            <tr>
                <td><a href="/">Главная</a> &rarr; &nbsp;</td>
                <td>Топ пользователей</td>
            </tr>
            </tbody>
        </table>
        <br>
        <div class="main_title" style="text-align: center; font-family: 'Times New Roman', Georgia; font-size: 23px;">
            Топ 10 пользователей
        </div>
        <table class="table-border">
            <tbody>
            <c:forEach var = "user" items = "${users}">
                <tr>
                <td class="msg-img"><img src="image/user.png" width="32" height="32" alt="theme"></td>
                <td><a href="#">${user.login}</a></td>
                <td style="width: 300px;">Дата регистрации: ${user.dateCreated}</td>
                <td class="count">Сообщений: ${user.messageCount}</td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </article>
</div>
