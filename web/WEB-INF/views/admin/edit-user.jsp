﻿<!DOCTYPE html>

<div class="wrapper">
    <article style="min-width: 700px">
        <table class="second-navigation">
            <tbody>
            <tr>
                <td><a href="/">Главная</a> &rarr; &nbsp;</td>
                <td>Редактирование пользователя</td>
            </tr>
            </tbody>
        </table>
        <br>
        <div class="registration" style="height:300px;">
            <div class="main_title">
                Редактирование никнейма пользователя
            </div>

            <div class="reg-form" style="height:87%;">
                <br>
                <form name="edit-user" action="/admin/edit-user" method="post">
                    <input type="hidden" name="currentPage" value="${id_user}">
                    <p >Введите изменения в логин:  <input  class="reg" type="text" maxlength="30" name="edit_user" value="${login}"></p>

                    <p >Введите изменения в логин:  <input  class="reg" type="password" maxlength="30" name="password" value="${password}"></p>

                    <select name="role" style="float: left; margin-left: 50%">
                        <option value="2">Админ</option>
                        <option value="0">Пользователь</option>
                    </select>
                    <br><br>
                    <button type="submit" style="margin-top: 0px; margin-left: 50%; float: left;">Редактировать</button>
                </form>
                <a href="#" onclick="history.back()"><button style="margin-top: 0px;margin-left: 15px;">Отмена</button></a>
            </div>

        </div>
        <br>
    </article>

</div>
