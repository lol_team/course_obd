﻿<%@ page import="db_Proccesing.Message" %>
<%@ page import="db_Proccesing.Topic" %>
<!DOCTYPE html>

		<div class="wrapper">
				<article style="min-width:900px">
					<table class="second-navigation">
					<tbody>
						<tr>
							<td><a href="/">Главная</a> &rarr; &nbsp;</td>
							<td>Админ панель</td>
						</tr>
					</tbody>
					</table>
				
					<br>
				
					
				<div id="add-theme" style="min-width:900px; height:100%;">
					<div class="main_title" style="height:70px; padding-left:3%; padding-right:3%; min-width:800px;">
						
						<div class="inset current-inset" style="float:left;">Основное</div>
						<a href="admin?show=users_control"><div class="inset" style="float:left;font-size: 0.7em">Управление пользователями</div></a>
						<a href="admin?show=headings_control"><div class="inset" style="float:left;">Управление рубриками</div></a>
						<a href="admin?show=filter"><div class="inset" style="float:left;">Фильтр нецензурных слов</div></a>

					</div>
					
						<div id="add-theme-form">	<br>
							<h3>Последние действия на форуме</h3>
							<p style="text-align:left">Последнее сообщение оставленно <%= Message.getUserLastMsg()%>, текст: <%= Message.getLastMsg() %></p>
							<p style="text-align:left">Последняя созданная тема:  <%= Topic.getLastTopic()%></p>
							<p style="text-align:left">Количество зарегистрированных пользователей: <%= Users_processing.getUsersCount()%></p>
							<p style="text-align:left">Новый пользователь: <%= Users_processing.getLastUser().getLogin()%></p>
							<p></p>
						</div>
						
				</div>
					<br>
				</article>
	</div>
