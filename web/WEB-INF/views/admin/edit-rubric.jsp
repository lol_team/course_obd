﻿<%@ page import="db_Proccesing.Heading" %>
<!DOCTYPE html>

<div class="wrapper">
    <article>
        <table class="second-navigation">
            <tbody>
            <tr>
                <td><a href="/">Главная</a> &rarr; &nbsp;</td>
                <td>Редактирование рубрики</td>
            </tr>
            </tbody>
        </table>
        <br>
        <div class="registration" style="height:200px; min-width: 500px">
            <div class="main_title">
                Редактирование наименования рубрики
            </div>

            <div class="reg-form" style="height:80%;">
                <br>
                <form name="edit-rubric" action="/admin/edit-rubric" method="post">
                    <input type="hidden" name="currentPage" value="${id_heading}">
                    <p style="margin-left: 20px;">Редактировать название:  <input  class="reg" type="text" maxlength="30" name="edit_rubr" value="<%= Heading.getLoginById(request.getQueryString().replace("show=headings_control&do=edit&id=","")) %>"></p>
                    <button type="submit" style="margin-top: 0px; margin-left: 50%; float: left;">Редактировать</button>
                </form>
                <a href="#" onclick="history.back()"><button style="margin-top: 0px;margin-left: 15px;">Отмена</button></a>
            </div>

        </div>
        <br>
    </article>

</div>
