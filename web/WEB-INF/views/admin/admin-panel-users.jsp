﻿<!DOCTYPE html>

		<div class="wrapper">
				<article style="min-width:900px;">
					<table class="second-navigation">
					<tbody>
						<tr>
                            <td><a href="/">Главная</a> &rarr; &nbsp;</td>
							<td>Админ панель</td>
						</tr>
					</tbody>
					</table>
				
					<br>
				
					
				<div id="add-theme" style="min-width:900px; height:100%; padding-bottom:10px;">
					<div class="main_title" style="height:70px; padding-left:3%; padding-right:3%; min-width:800px;">
						
						<a href="/admin"><div class="inset" style="float:left;">Основное</div></a>
						<div class="inset current-inset" style="float:left; font-size: 0.7em">Управление пользователями</div></a>
						<a href="admin?show=headings_control"><div class="inset" style="float:left;">Управление рубриками</div></a>
						<a href="admin?show=filter"><div class="inset" style="float:left;">Фильтр нецензурных слов</div></a>

					</div>
					
						<div id="add-theme-form">	<br>
							<h3>Список пользователей:</h3>
							<table class="table-border" style="text-align:left;">
							<tbody>
                            <c:forEach var = "user" items = "${users}">
                                <tr>
                                    <td class="msg-img"><img src="image/admin.png" width="32" height="32" alt="theme"></td>
                                    <td><a href="#">${user.login}</a></td>
                                    <td style="width:200px;">
                                        <a class="ico delete-user" href="admin?show=users_control&do=delete&id=${user.id}">Удалить</a>
                                        <a class="ico edit"        href="admin?show=users_control&do=edit&id=${user.id}">Изменить</a>
                                    </td>
                                </tr>
                            </c:forEach>
							</tbody>
						</table>
					<br><br>
                            <div style="width: 500px;margin: auto">
							<form action="/admin" method="post">
								<fieldset>
									<legend>Добавить нового пользователя</legend><br>
									<p style="text-indent:15px">Имя пользователя:  <input  class="reg" type="text" maxlength="20" name="login"></p>
									<p  style="text-indent:89px">Пароль: <input class ="reg" type="password" name="password" maxlength="20"></p>
                                    <input type="hidden" name="currentPage" value="<%=request.getRequestURL().append('?').append(request.getQueryString())%>">
                                       <p style="margin-left: 30%;"> <select name="role">
                                            <option value="2">Админ</option>
                                            <option value="0">Пользователь</option>
                                        </select></p>
                                    <button style="float:right;" type="reg">Зарегистрировать</button>
								</fieldset>
							</form>
                            </div>
						</div>
						
				</div>
					<br>
				</article>
	</div>
