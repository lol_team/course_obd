﻿
		<div class="wrapper">
				<article style="min-width:900px;">
					<table class="second-navigation">
					<tbody>
						<tr>
                            <td><a href="/">Главная</a> &rarr; &nbsp;</td>
							<td>Админ панель</td>
						</tr>
					</tbody>
					</table>
				
					<br>
				
					
				<div id="add-theme" style="min-width:900px; height:100%; padding-bottom:10px;">
					<div class="main_title" style="height:70px; padding-left:3%; padding-right:3%; min-width:800px;">
						
						<a href="/admin"><div class="inset" style="float:left;">Основное</div></a>
						<a href="admin?show=users_control"><div class="inset" style="float:left;font-size: 0.7em">Управление пользователями</div></a>
						<a href="admin?show=headings_control"> <div class="inset" style="float:left;">Управление рубриками</div></a>
						<div class="inset current-inset" style="float:left;">Фильтр нецензурных слов</div>

					</div>
					
						<div id="add-theme-form">	<br>
							<h3>Список нецензурных слов:</h3>
							<table class="table-border" style="text-align:left;">
							<tbody>
                            <c:forEach var = "matyuk" items = "${words}">
                                <tr>
                                    <td class="msg-img"><img src="image/ms.png" width="32" height="32" alt="theme"></td>
                                    <td><a href="#">${matyuk.word}</a></td>
                                    <td style="width:200px;">
                                        <a class="ico delete-word" href="admin?show=filter&do=delete&id=${matyuk.id}">Удалить</a>
                                        <a class="ico edit"        href="admin?show=filter&do=edit&id=${matyuk.id}"> Изменить</a>
                                    </td>
                                </tr>
                            </c:forEach>

							</tbody>
						</table>
					<br><br>
							<form action="/admin" method="post">
								<fieldset>
									<legend>Добавить нецензурное слово</legend><br>
									Введите слово  &nbsp;<input style="width:400px; text-indent:3px;" class="titleNewTheme" type="text" maxlength="50" value="" name="word">
                                    <input type="hidden" name="currentPage" value="<%=request.getRequestURL().append('?').append(request.getQueryString())%>">
                                    <button style="float:right;" type="add">Добавить</button>
								</fieldset>
							</form>
						</div>
						
				</div>
					<br>
				</article>
	</div>
