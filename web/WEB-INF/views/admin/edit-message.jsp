<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="wrapper">
    <article>
        <table class="second-navigation">
            <tbody>
            <tr>
                <td><a href="/">Главная</a> &rarr; &nbsp;</td>
                <td>Редактирование сообшения</td>
            </tr>
            </tbody>
        </table>

        <h2>Редактирование сообщения</h2>


        <div id="add-theme">
            <div class="main_title">
                Измененное сообщение
            </div>

            <div id="add-theme-form">
                <br>
                <form name="edit_message" action="/admin" method="post">
                    <textarea style="width:95%; background-color:rgb(255, 255, 255);" placeholder="Введите сообщение..." name="message">${message}</textarea>
                    <input type="hidden" name="topic_id" value="${topic_id}">
                    <input type="hidden" name="message_id" value="${id_message}">
                    <button style="margin-left:2%;" type="submit">Редактировать</button>
                </form>
                <a href="#" onclick="history.back()"><button style="margin-left: 15px;">Отмена</button></a>
            </div>

        </div>
        <br>
    </article>
</div>
