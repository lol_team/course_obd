﻿<!DOCTYPE html>

		<div class="wrapper">
				<article>
					<table class="second-navigation">
					<tbody>
						<tr>
                            <td><a href="/">Главная</a> &rarr; &nbsp;</td>
							<td>Редактирование слова</td>
						</tr>
					</tbody>
					</table>
				<br>
				<div class="registration" style="height:200px; min-width: 500px;max-width: 1200px">
					<div class="main_title">
						Редактирование нецензурного слова
					</div>
					
						<div class="reg-form" style="height:80%;">
						<br>
							<form name="edit-word" method="post" action="/admin/edit-word">
								<p style="margin-left: 20px;">Редактировать слово:  &nbsp;&nbsp;<input  class="reg" type="text" maxlength="30" name="edit_word" value="${word}"></p>
								<input type="hidden" name="currentPage" value="${id_word}">
								<button type="submit" style="margin-top:0px;margin-left: 48.5%;float: left;">Редактировать</button>
							</form>
							<a href="#" onclick="history.back()"><button style="margin-top: 0px;margin-left: 15px;">Отмена</button></a>

						</div>
						
				</div>
					<br>
				</article>

	</div>
