﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>

<div class="wrapper">
    <article>
        <table class="second-navigation">
            <tbody>
            <tr>
                <td><a href="/">Главная</a> &rarr; &nbsp;</td>
                <td><a href="/showforum?id=${id_topics_heading}">${name_topics_heading}</a>&rarr; &nbsp;</td>
                <td><a href="/showtopic?id=${current_topic}">${name_topic}</a>&rarr; &nbsp;</td>
            </tr>
            </tbody>
        </table>
        <br>


        <!-- Данный участок кода в админку пришить. То что в данном участке для обычных юзеров - заменить на это-->
        <c:if test="<%= Users_processing.isUserAdmin(Users_processing.getLoginFromCookies(request)) %>">
            <form name="edit-theme" style="float:right;" action="/admin/topicController" method="get">
                <input style="height:23px; text-indent:3px; width:250px;" class="reg" type="text" maxlength="40"
                       name="new_name" placeholder="Новое название темы">
                <input type="hidden" name="topic_id" value="${current_topic}">

                <button style="margin-top:0px; margin-left:20px; float:right;" type="submit" name="change_name"
                        value="ok">Изменить
                </button>
            </form>
            <br><br><br>

            <form name="edit-theme" style="float:right;" action="/admin/topicController" method="get">
                <input type="hidden" name="topic_id" value="${current_topic}">

                <p> Переместить тему: &nbsp; <select name="to_heading">
                    <c:forEach var="heading" items="${headings}">
                        <option value="${heading.id}">${heading.name}</option>
                    </c:forEach>
                </select>
                    <button style="margin-top:0px; margin-left:20px; float:right;" type="submit" name="replace">
                        Переместить
                    </button>
                </p>

            </form>
            <br><br><br>
            <a href="/admin/topicController?delete=${current_topic}"><button style="margin-top:0px; margin-left:20px; float: right;">Удалить тему</button></a>
            <c:if test="${status == true}">
            <a href="/admin/topicController?flag=${current_topic}&id=0"><button style="margin-top:0px; margin-left:20px; float: right;">Закрыть тему</button></a>
                </c:if>
            <c:if test="${status == false}">
                <a href="/admin/topicController?flag=${current_topic}&id=1"><button style="margin-top:0px; margin-left:20px; float: right;">Открыть тему</button></a>
            </c:if>

        </c:if>
        <!-- END-->

        <h2>${name_topic}</h2>

        <div class="main_title" style="font-size:0.8em; font-weight:normal;">
            Сообщений в теме:
        </div>

        <c:forEach var="message" items="${messages}">
            <table class="table-border">
                <tbody>
                <div class = "topic-date" >
                        ${message.date_message}
                </div>
                <tr>
                    <td class="topic-author">${message.nickName}</td>
                    <td >${message.text}</td>
                </tr>
                <!-- В админку для каждого сообщения -->
                <c:if test="<%= Users_processing.isUserAdmin(Users_processing.getLoginFromCookies(request)) %>">

                    <tr>
                        <td style="border:none;"></td>
                        <td style="border:none;">

                            <a href="/admin?show=message&do=edit&id=${message.id_message}&topic=${current_topic}">
                                <button style="margin-top:0px; margin-left:0px;">Редактировать</button>
                            </a>
                            <a href="/admin?show=message&do=delete&id=${message.id_message}&topic=${current_topic}">
                                <button style="margin-top:0px; margin-left:15px;">Удалить</button>
                            </a>
                        </td>
                    </tr>
                </c:if>
                <!-- END -->
                </tbody>
            </table>
        </c:forEach>
        <c:if test="<%= Users_processing.isUserAuth(request) %>">
            <c:if test="${status == true}">
                <div class="txt-msg">
                    <p>Введите сообщение:</p>

                    <form name="text-message" class="button-message" action="addMsg" method="post">
                        <textarea name="message_txt" wrap="hard"></textarea>

                        <input type="hidden" name="currentPage"
                               value="${current_topic}">

                        <p>
                            <button style="float:right;" type="submit">Ответить</button>
                        </p>
                        <br><br>
                    </form>
                </div>
            </c:if>
        </c:if>
        <c:if test="${status == false}">
            <div class="txt-msg">
                <p>Тема закрыта!</p>
            </div>
        </c:if>
        <c:if test="<%= !Users_processing.isUserAuth(request) %>">
            <div class="txt-msg">
                <p>Вы не авторизированы!</p>
            </div>
        </c:if>
    </article>
</div>
