
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="wrapper">
    <article>
        <aside>
            <div class="main_title">
                Последние события
            </div>
            <div id="menu">
                <p>Последнее сообщение: <a href="/showtopic?id=${last_message_topic}">${last_message_text}</a></p>
                <p>Поприветствуем пользователя: ${last_login}</p>
                <p>Зарегистрированных пользователей: ${users_count}</p>

            </div>
        </aside>
        <div class = "content">
        <div class="main_title">
            Форум для курсового проекта по проектированию БД
        </div>
        <table class="table-border">
            <tbody>

            <c:forEach var = "heading" items = "${headings}">
                <tr>
                    <td class="msg-img"><img src="image/message.png" width="32" height="32" alt="theme"></td>
                    <td><a href="showforum?id=${heading.id}" title=${heading.name}>${heading.name}</a></td>
                    <td class="count">${heading.themeCount} Тем <br> Сообщений:</td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
        <br>
        </div>
    </article>

</div>
