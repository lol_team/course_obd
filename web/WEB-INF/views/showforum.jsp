﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="wrapper">
				<article>
					<table class="second-navigation">
					<tbody>
						<tr>
                            <td><a href="/">Главная</a> &rarr; &nbsp;</td>
                            <td><a href="/showforum?id=${id_heading}">${name_heading}</a>&rarr; &nbsp;</td>
						</tr>
                    <br><br>
					</tbody>
					</table>
                    <c:if test = "<%= Users_processing.valid_user(Users_processing.getLoginFromCookies(request)) || Users_processing.isUserAdmin(Users_processing.getLoginFromCookies(request)) %>">
					<h4 style="float:right"><a href="addTopic?id_heading=${id_heading}" title="Добавить новую тему">Новая тема</a></h4>
                    </c:if>
					<table class="table-border">
							<tbody>

                                <c:forEach var = "topic" items = "${topics}">
                                    <tr>
                                        <td class="msg-img"><img src="image/msg-topic.png" width="32" height="32" alt="theme"></td>
                                        <td><a href="showtopic?id=${topic.id}" title="${topic.name}">${topic.name}</a></td>
                                        <td class="count">Автор ... <br> Сообщений: ${topic.messageCount}</td>
                                    </tr>
                                </c:forEach>
							</tbody>
						</table>
				</article>
	</div>