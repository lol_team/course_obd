<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title> Ошибка 404 | Database Forum</title>
    <link rel="shortcut icon" href="image/icon.png" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="style.css">


</head>

<body>

<div class="error-404"></div>
<h2 style="font-family:Garamond; color:rgb(15, 15, 110); text-align:center;">К сожалению данной страницы не существует. <br>Возможно она была удалена, или Вы неправильно ввели адрес.</h2>
<a style="display:block; width:100px; margin:auto;" href="/" title="На главную">
    <img src="image/button.png" height="48" width="119" alt="Домой">
</a>

</body>



</html>